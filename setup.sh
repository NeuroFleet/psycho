#!/bin/bash

LOCALE=en_US.UTF-8

TARGET=/psy

REALMs="common connector console explor linked hobbit linguist devops infosec"

GIT_PREFIX=https://bitbucket.org

#********************************************************************

PKGs="build-essential git unzip unrar-free"
PKGs=`echo $PKGs python-{virtualenv,pip,dev}`

#####################################################################

sudo localectl set-locale LANG=$LOCALE,LC_ALL=$LOCALE

locale-gen

export LANG=$LOCALE
export LC_ALL=$LOCALE

#********************************************************************

apt update

apt -y upgrade

apt install -y --force-yes `echo $PKGs`

#********************************************************************

if [[ ! -d $TARGET ]] ; then
    git clone $GIT_PREFIX/NeuroFleet/psycho.git $TARGET --recursive
fi

for key in $REALMs ; do
    if [[ ! -d $TARGET/realms/$key ]] ; then
        git clone $GIT_PREFIX/NeuroFleet/$key.git $TARGET/realms/$key --recursive
    fi
done

#####################################################################

cd $TARGET

source environ.sh

# ./builder --cache 
# ./builder --cache --hdt --nltk


from gestalt.crawler.shortcuts import *

################################################################################

class Spider(CrawlEngine):
    def visit(self, link):
        print '\t-> visited:', repr(link.url), 'from:', link.referrer

    def fail(self, link):
        print '\t-> failed:', repr(link.url)

    #***************************************************************************

    def extract(self, doc, schema, match):
        print '\t-> extract: %s | %s' % (link, match)

        for field in schema['fields']:
            response = doc

            if 'expr' in field:
                try:
                    field['value'] = eval(field['expr'])
                except Exception,ex:
                    field['value'] = match

                    #field['value'] = '! (%s) ? %s' % (field['expr'], ex)

                print "\t\t =) %(name)s \t: %(value)s" % field

            #elif 'dict' in field:
            #    print "\t\t #) %(name)s \t: %(dict)s" % field

            elif 'css' in field:
                field['value'] = doc.css(field['css'])

                print "\t\t *) %(name)s \t: %(value)s" % field

            elif 'xpath' in field:
                field['value'] = doc.xpath(field['xpath'])

                print "\t\t *) %(name)s \t: %(value)s" % field

    def process(self, entry):
        pass


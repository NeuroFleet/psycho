var Catalog = {
    logger: {
        pubsub: function (topic, args) {
            // console.log("PUBSUB:>\t"+topic+" with args : ", args);

            var uid = Program.UI.notify({
		        title: '(PubSub) '+topic,
		        text: JSON.stringify(args),
		        sticky: false,
		        time: '3s',
		        class_name: 'pubsub-message',
	        });
        },
    },
    callbacks: [
        function () {
        },
    ],
    methods: {
        
    },
    feeds: {
        'reactor.console': function (args) {
            Catalog.logger.pubsub('reactor.logging', args);
        },
        'reactor.process': function (args) {
            Catalog.logger.pubsub('reactor.execute', args);
        },
        /*
        'reactor.head.face.mouth.rdf.import': function (args) {
            Catalog.logger.pubsub('reactor.head.face.mouth.rdf.import', args);
        },
        'reactor.head.face.mouth.sparql.log': function (args) {
            Catalog.logger.pubsub('reactor.head.face.mouth.sparql.log', args);
        },
        'reactor.head.face.mouth.tpf.console': function (args) {
            Catalog.logger.pubsub('reactor.head.face.mouth.tpf.console', args);
        },

        'reactor.head.face.mouth.saying': function (args) {
            Catalog.logger.pubsub('reactor.head.face.mouth.saying', args);
        },
        'reactor.head.face.mouth.dyslex': function (args) {
            Catalog.logger.pubsub('reactor.head.face.mouth.dyslex', args);
        },
        'reactor.head.face.mouth.jabber': function (args) {
            Catalog.logger.pubsub('reactor.head.face.mouth.jabber', args);
        },

        'reactor.head.face.eye.left.face': function (args) {
            Catalog.logger.pubsub('reactor.head.face.eye.left.face', args);
        },
        //*/
    },
};

$(function() {
    Program.IO.conn = new autobahn.Connection(Config.endpoint);

    Program.IO.conn.onopen  = Program.IO.on_open;
    Program.IO.conn.onclose = Program.IO.on_close;

    Program.IO.conn.open();

    // App.init();
});


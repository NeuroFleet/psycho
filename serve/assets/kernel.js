AUTOBAHN_DEBUG = true;

var Program = {
    main: null,
    IO: {
        publish: function (topic, args) {
            if (Program.IO.sess!=null) {
                return Program.IO.sess.publish(topic, args);
            } else {
                return null;
            }
        },
        invoke: function (method, args, callback, handler) {
            if (Program.IO.sess!=null) {
                return Program.IO.sess.call(method, args);
            } else {
                return null;
            }
        },
        conn: null,
        sess: null,
        on_open: function (session) {
            $.each([
                Catalog.feeds,
                Program.topics,
            ], function (index,listing) {
                if (listing!=null) {
                    $.each(listing, function (alias,handler) {
                        session.subscribe(alias,handler).then(function (res) {
                            console.log("\t*) Subscribed to topic '"+alias+"' ...");
                        }, function (err) {
                            console.log("\t#) Failed to subscribe to topic '"+alias+"' : ", err);
                        });
                    });
                }
            });

            $.each([
                Catalog.methods,
                Program.methods,
            ], function (index,listing) {
                if (listing!=null) {
                    $.each(listing, function (alias,handler) {
                        session.register(alias,handler).then(function (res) {
                            console.log("\t*) Registered procedure '"+alias+"' ...");
                        }, function (err) {
                            console.log("\t#) Failed to register procedure '"+alias+"' : ", err);
                        });
                    });
                }
            });

            Program.IO.sess = session;

            $.each([
                Catalog.callbacks,
                Program.UI.callbacks,
                Program.callbacks,
            ], function (index,listing) {
                var handler;

                for (var i=0 ; i<listing.length ; i++) {
                    handler = listing[i];

                    handler();
                }
            });

            if (Program.main!=null) {
                console.log("Running Program.main() ...");

                Program.main();
            } else {
                console.log("No 'main()' method defined as Program.");
            }
        },
        on_close: function (reason, details) {
            Program.IO.sess = null;

            console.log("Connection lost: ", reason, details);

            /*
            if (t1) {
               clearInterval(t1);
               t1 = null;
            }
            if (t2) {
               clearInterval(t2);
               t2 = null;
            }
            //*/
        },
    },
    UI: {
        notify: function (cfg) {
            cfg.sticky     = cfg.sticky     || false;
            cfg.time       = cfg.time       || '3s';
            cfg.class_name = cfg.class_name || 'pubsub';

            var uid = $.gritter.add(cfg);

            return uid;
        },
        graph: function (narrow, data, options) {
            var container = document.getElementById(narrow);

            var network = new vis.Network(container, data, options);

            return network;
        },
        tree: function (narrow, data, options) {
        },
        chrono: function (narrow, data, options) {
        },
        callbacks: [],
        widgets: {},
    },
};

var Graph = {
    topologies: {
        default: {
            nodes: {
                shape: 'dot', size: 20,
                font: { size: 15, color: '#ffffff' },
                borderWidth: 2
            },
            edges: {
                width: 2
            },
            groups: {
                url: {
                    color: {background:'red',border:'white'},
                    shape: 'diamond'
                },
                dotsWithLabel: {
                    label: "I'm a dot!",
                    shape: 'dot',
                    color: 'cyan'
                },
                mints: {color:'rgb(0,255,140)'},
                icons: {
                    shape: 'icon',
                    icon: {
                        face: 'FontAwesome',
                        code: '\uf0c0',
                        size: 50,
                        color: 'orange'
                    }
                },
                source: {
                    color:{border:'white'}
                }
            }
        },
        agora: {
            nodes: {
                size: 20,
                font: {
                    size: 15,
                    color: '#000000'
                },
                borderWidth: 2
            },
            edges: {
                width: 2,
                color: '#000000'
            },
            groups: {
                first:  { color: 'red' },
                second: { color: 'yellow' },
                third:  { color: 'green' },
                forth:  { color: 'blue' },
                fifth:  { color: 'purple' },
                sixth:  { color: 'pink' },
            }
        },
    },
};


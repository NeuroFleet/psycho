@cli.command('invite')
@click.option('-q', '--queue', multiple=True)
@click.pass_context
def shell_invite(ctx, *args, **kwargs):
    from bpython.cli import main

    del sys.argv[1]

    @shell_dir_exts('invite.py', globals(), locals())
    def load_shell_exts(path, spec, glob, locl):
        source = open(path).read()

        try:
            exec source in glob, locl
        except Exception,ex:
            print "Error raised while parsing : %s" % path

            raise ex

    sys.exit(main(locals_=locals()))

#***************************************************************************************

@cli.command('execute')
@click.pass_context
def execute_command(ctx, *args, **kwargs):
    os.system('rqscheduler -v')

#***************************************************************************************

@cli.command('process')
@click.pass_context
def process_daten(ctx, *args, **kwargs):
    os.system('rqscheduler -v')


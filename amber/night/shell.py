@cli.command('worker')
@click.option('-q', '--queue', default="default")
#@click.option('-q', '--queue', multiple=True)
@click.pass_context
def run_worker(ctx, queue, *args, **kwargs):
    from rq import Connection, Worker

    with Connection() as conn:
        w = Worker(queue)

        w.work()

#***************************************************************************************

@cli.command('clocks')
@click.option('-i', '--interval', type=float)
@click.pass_context
def run_scheduler(ctx, *args, **kwargs):
    os.system('rqscheduler -i %(interval)s -v' % kwargs)

#***************************************************************************************

@cli.command('implant')
@click.argument('endpoint', default=u"ws://apis.uchikoma.faith:8000/io-sockets")
@click.option('--realm', '-r', default="reactor")
@click.option('--prefix', '-p', default="reactor.webapp.common")
@click.option('--middleware', '-mw', multiple=True)
#@click.option('--suffix', '-s', default="")
@click.pass_context
#def run_implant(ctx, endpoint, realm, prefix, middleware, suffix, *args, **kwargs):
def run_implant(ctx, endpoint, realm, prefix, middleware, *args, **kwargs):
    from autobahn.twisted.wamp import ApplicationRunner

    runner = ApplicationRunner(unicode(endpoint), unicode(realm))

    target = []

    for key in middleware:
        hnd = __import__('%s.streams' % prefix)

        for sub in key.split('.'):
            if hnd!=None:
                if hasattr(hnd, sub):
                    hnd = getattr(hnd, sub)
                else:
                    hnd = None

        if issubclass(hnd, object):
            target.append(hnd)

    for mw in target:
        runner.run(mw)


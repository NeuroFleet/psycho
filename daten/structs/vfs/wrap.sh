#!/bin/bash

MOUNTp=/media/tayamino

ROOTp=$MOUNTp/VAULT/Persist

cd $ROOTp

if [[ ! -f docker-compose.yaml ]] ; then rm -f docker-compose.yaml ; fi

for key in `ls $ROOTp` ; do
    cat $ROOTp/$key/docker-compose.yaml | sed -e "s/\/media\/theque/"$MOUNTp"/g" >>docker-compose.yaml
done

TARGETs=`echo vfs-{connector,console,linked,infosec,linguist,devops,explor,hobbit}`

#docker-compose up -d $TARGETs

cat docker-compose.yaml


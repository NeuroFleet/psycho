#!/bin/bash

TARGET=/media/theque/SysOp/ROOT

#for ep in connector console linked infosec linguist devops explor hobbit ; do
for ep in linked infosec linguist explor hobbit ; do
    for buck in `ls $TARGET/$ep` ; do
        case $buck in
            .|..)
                ;;
            *)
                mc mb -q vfs-$ep/$buck

                mc mirror $TARGET/$ep/$buck/ vfs-$ep/$buck/
                ;;
        esac
    done

    for buck in `mc ls vfs-$ep/ | sed "s/\(.*\)0B \(.*\)\//\2/"` ; do
        if [[ ! -d $TARGET/$ep/$buck ]] ; then
            mkdir -p $TARGET/$ep/$buck
        fi

        mc mirror vfs-$ep/$buck/ $TARGET/$ep/$buck/
    done
done


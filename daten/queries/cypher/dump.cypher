CREATE (Memcache:DockerImage { image: "memcached", ports: [ 11211 ], roles: [ "key-value", "cache" ] }),
       (Redis:DockerImage { image: "redis", ports: [ 6379 ], volumes: [
           "/srv/data/backends/cache/redis:/data"
       ], roles: [ "key-value", "cache", "queue" ] }),
       (LevelDB:DockerImage { image: "ekristen/leveldb", ports: [ 2012 ], volumes: [
           "/srv/data/backends/cache/level:/opt/app/db"
       ], roles: [ "key-value", "cache", "big-data" ] }),

       (RabbitMQ:DockerImage { image: "rabbitmq:3", ports: [ 4369, 5671, 5672, 6379, 25672 ], volumes: [
           "/srv/data/backends/stream/amqp:/data"
       ], environ: [
           "POSTGRES_USER=odoo", "POSTGRES_PASSWORD=odoo", "POSTGRES_DB=odoo"
       ], roles: [ "queue", "pubsub" ] }),
       (Mosquitto:DockerImage { image: "ansi/mosquitto", ports: [ 1883 ], volumes: [
           "/srv/data/backends/stream/mqtt:/data"
       ], roles: [ "pubsub" ] }),
       (ApacheKafka:DockerImage { image: "spotify/kafka", ports: [ 2181, 9092 ], volumes: [
           "/srv/data/backends/stream/mqtt:/data"
       ], environ: [
           "ADVERTISED_HOST", "172.17.42.1", "ADVERTISED_PORT", "9092"
       ], roles: [ "queue", "pubsub" ] }),

       (PostgreSQL:DockerImage { image: "postgres:9.4", ports: [ 5432 ], volumes: [
           "/ronin/var/backends/database/biz-postgres:/var/lib/postgresql/data"
       ], environ: [
           "POSTGRES_USER=odoo", "POSTGRES_PASSWORD=odoo", "POSTGRES_DB=odoo"
       ], roles: [ "sql", "database" ] }),
       (MySQL:DockerImage { image: "mysql:5.7", ports: [ 3306 ], volumes: [
           "/ronin/var/backends/queue/results:/var/lib/mysql"
       ], environ: [
           "MYSQL_ROOT_PASSWORD=oklawamina", "MYSQL_DATABASE=uchikoma_proto"
       ], roles: [ "sql", "database" ] }),

       (Solr:DockerImage { image: "solr", ports: [ 8983 ], volumes: [
           "/ronin/var/backends/store/indexes:/opt/solr/server/solr/mycores"
       ], roles: [ "index" ] }),
       (ElasticSearch:DockerImage { image: "elasticsearch", ports: [ 9200, 9300 ], volumes: [
           "/ronin/var/backends/store/elastic:/usr/share/elasticsearch/data"
       ], roles: [ "index" ] }),

       (MongoDB:DockerImage { image: "mongo", ports: [ 27017 ], volumes: [
           "/srv/data/backends/mongo:/data/db"
       ], roles: [ "nosql", "database" ] }),
       (CouchDB:DockerImage { image: "couchdb", ports: [ 5984 ], volumes: [
           "/srv/data/backends/couch:/usr/local/var/lib/couchdb"
       ], environ: [
           "COUCHDB_USER=root", "COUCHDB_PASSWORD=toor"
       ], roles: [ "nosql", "database" ] }),

       (Neo4j:DockerImage { image: "neo4j", ports: [ 1337, 7474, 7687 ], volumes: [
           "/srv/data/backends/graph:/opt/data"
       ], roles: [ "graph", "database", "traversal" ] }),
       (AllegroGraph:DockerImage { image: "apiwise/allegrograph", ports: [ 10035 ], volumes: [
           "/srv/data/backends/graph:/opt/data"
       ], roles: [ "graph", "database", "traversal" ] }),

       (JenaFuseki:DockerImage { image: "stain/jena-fuseki", ports: [ 8080 ], volumes: [
           "/srv/data/backends/graph:/opt/data"
       ], roles: [ "rdf", "database", "traversal", "sparql" ] }),
       (iServe:DockerImage { image: "openuniversity/iserve", ports: [ 8080 ], volumes: [
           "/srv/data/backends/graph:/opt/data"
       ], roles: [ "rdf", "database", "traversal", "sparql" ] }),
       (Virtuoso:DockerImage { image: "virtuoso", ports: [ 8080 ], volumes: [
           "/srv/data/backends/graph:/opt/data"
       ], roles: [ "rdf", "database", "traversal", "sparql" ] }),

       (ApacheCassandra:DockerImage { image: "cassandra", ports: [
           7000, 9042
       ], volumes: [
           "/ronin/var/backends/store/records:/var/lib/cassandra"
       ], environ: [
           "CASSANDRA_LISTEN_ADDRESS=0.0.0.0", "CASSANDRA_BROADCAST_ADDRESS=66.228.51.39",
           "CASSANDRA_CLUSTER_NAME=uchikoma-proto"
       ], roles: [ "key-value", "big-data" ] }),

       (ApacheHadoop:DockerImage { image: "sequenceiq/hadoop-docker:2.7.0", ports: [
            2122, 19888, 49707, 50010, 50020, 50070, 50075, 50090,
            8030, 8031, 8032, 8033, 8040, 8042, 8088
       ], volumes: [
           "/ronin/var/backends/store/records:/var/lib/cassandra"
       ] }),

       (ApachePig:DockerImage { image: "apache2use/pig", ports: [ 8000 ], volumes: [
           "/ronin/var/backends/store/records:/var/lib/cassandra"
       ], roles: [ "big-data", "bulk-exec" ] }),
       (ApacheSpark:DockerImage { image: "sequenceiq/spark:1.6.0", ports: [
           8042, 8088
       ], volumes: [
           "/ronin/var/backends/store/records:/var/lib/cassandra"
       ], roles: [ "big-data", "machine-learning" ] }),

       (ApacheDrill:DockerImage { image: "apache2use/drill", ports: [ 8000 ], volumes: [
           "/ronin/var/backends/store/records:/var/lib/cassandra"
       ], roles: [ "sql", "database" ] }),
       (ApacheGiraph:DockerImage { image: "apache2use/giraph", ports: [ 8000 ], volumes: [
           "/ronin/var/backends/store/records:/var/lib/cassandra"
       ] }),
       (ApacheMarmotta:DockerImage { image: "apache2use/marmotta", ports: [ 8000 ], volumes: [
           "/ronin/var/backends/store/records:/var/lib/cassandra"
       ], roles: [ "rdf", "database", "traversal", "sparql" ] }),
       (ApachePhoenix:DockerImage { image: "apache2use/phoenix", ports: [ 8000 ], volumes: [
           "/ronin/var/backends/store/records:/var/lib/cassandra"
       ] }),
       (ApacheStanbol:DockerImage { image: "apache2use/stanbol", ports: [ 8000 ], volumes: [
           "/ronin/var/backends/store/records:/var/lib/cassandra"
       ], roles: [ "rdf", "nlp", "sparql" ] }),

       (Kamailio:DockerImage { image: "voipnovatos/kamailio", ports: [
           "5060:5080/udp"
       ], volumes: [
       ], environ: [
           "ADVERTISED_IP=10.0.100.26"
       ], roles: [ "voip", "talk", "comm" ], restart: "always", hostname: "srv-voip" }),
       (eJabberd:DockerImage { image: "rroemhild/ejabberd", ports: [
           5222, 5269, 5280
       ], volumes: [
       ], environ: [
           "XMPP_DOMAIN=it-issal.top",
           "ERLANG_NODE=ejabberd",
           "EJABBERD_ADMINS=admin@example.de admin2@it-issal.top",
           "EJABBERD_USERS=admin@example.de:password1234 admin2@it-issal.top",
           "TZ=Africa/Casablanca"
       ], roles: [ "xmpp", "talk", "comm" ], restart: "always", hostname: "srv-talk" }),

       (Odoo:DockerImage:BusinessMgmt { image: "business2use/odoo:8.0", ports: [
           8069, 8071
       ], volumes: [
           "/ronin/usr/addons/odoo:/mnt/extra-addons"
       ], roles: [ "business", "mgmt" ], hostname: "srv-erp" }),
       (Bonita:DockerImage:BusinessMgmt { image: "bonita", ports: [
           8080
       ], volumes: [
           "/ronin/var/run/biz/bonita:/opt/bonita"
       ], roles: [ "business", "mgmt" ], hostname: "srv-workflow" }),

       (Redmine:DockerImage:BusinessTool { image: "redmine", ports: [
           3000
       ], volumes: [
           "/ronin/var/run/biz/redmine:/usr/src/redmine/files"
       ], roles: [ "business", "mgmt" ], hostname: "srv-collab" }),

       (OwnCloud:DockerImage:FileServer { image: "owncloud", ports: [
           80
       ], volumes: [
           "/ronin/etc/biz/redmine:/var/www/html/config",
           "/ronin/usr/addons/owncloud:/var/www/html/apps",
           "/ronin/var/run/biz/owncloud:/usr/src/redmine/data"
       ], roles: [ "storage", "drives" ], hostname: "srv-drive" }),

       (OpenKM:DockerImage:BusinessTool { image: "mcsaky/openkm", ports: [
           8080
       ], volumes: [
            "/srv/data/mgmt-docs:/code"
       ], roles: [ "storage", "drives" ], hostname: "srv-docs" }),

       (OpenGTS:DockerImage:BusinessTool:GeoTool:GPS { image: "mcsaky/opengts", ports: [
           8080
       ], volumes: [
       ], roles: [ "storage", "drives", "geo" ], hostname: "srv-gts" }),
       (TracCar:DockerImage:BusinessTool:GeoTool:GPS { image: "daemon2use/traccar", ports: [
           8080
       ], volumes: [
       ], roles: [ "storage", "drives", "geo" ], hostname: "srv-gts" }),

       (EclipseChe:DockerImage:IDE:DevTool { image: "eclipse/che:5.0.0-latest", ports: [
           3000
       ], volumes: [
           "/ronin/srv/it-issal/workspace:/home/user/che",
           "/var/run/docker.sock:/var/run/docker.sock"
       ], roles: [ "storage", "drives" ], hostname: "dev-eclipse" }),

       (Jenkins:DockerImage:DevOps:CI { image: "jenkins", ports: [
           80
       ], volumes: [
       ], roles: [ "devops", "ci" ], hostname: "ci-builds" }),
       (StriderCD:DockerImage:DevOps:CI { image: "strider/strider", ports: [
           80
       ], volumes: [
       ], roles: [ "devops", "ci" ], hostname: "ci-ship" }),

       (Graphite:DockerImage:DevOps:Monitor { image: "graphite", ports: [
           80
       ], volumes: [
       ], roles: [ "devops", "monitor" ], hostname: "ops-metric" }),
       (Logstash:DockerImage:DevOps:Monitor { image: "logstash", ports: [
           80
       ], volumes: [
       ], roles: [ "devops", "monitor" ], hostname: "ops-logs" }),
       (Sentry:DockerImage:DevOps:Monitor { image: "sentry", ports: [
           80
       ], volumes: [
       ], roles: [ "devops", "monitor" ], hostname: "ops-watch" }),

       (SyslogNG:DockerImage:DevOps:Monitor { image: "balabit/syslog-ng:latest", ports: [
           80
       ], volumes: [
           "/ronin/etc/ops/syslog-ng.conf:/etc/syslog-ng/syslog-ng.conf"
       ], roles: [ "devops", "monitor" ], hostname: "ops-watch" }),
       (Munin:DockerImage:DevOps:Monitor { image: "munin", ports: [
           80
       ], volumes: [
           "/ronin/var/logs/ops-munin:/var/log/munin",
           "/ronin/var/ops/munin/munin/lib:/var/lib/munin",
           "/ronin/var/ops/munin/munin/run:/var/run/munin",
           "/ronin/var/ops/munin/munin/cache:/var/cache/munin"
       ], roles: [ "devops", "monitor" ], hostname: "ops-watch" }),

(ApacheDrill)-[:DEPENDS { alias: "hive" }]->(ApacheHadoop),
(ApacheGiraph)-[:DEPENDS { alias: "hive" }]->(ApacheHadoop),
(ApacheMarmotta)-[:DEPENDS { alias: "hive" }]->(ApacheHadoop),
(ApachePhoenix)-[:DEPENDS { alias: "hive" }]->(ApacheHadoop),
(ApachePig)-[:DEPENDS { alias: "hive" }]->(ApacheHadoop),
(ApacheSpark)-[:DEPENDS { alias: "hive" }]->(ApacheHadoop),

(Odoo)-[:DEPENDS { alias: "db" }]->(PostgreSQL),
(Bonita)-[:DEPENDS { alias: "db" }]->(PostgreSQL),

(Redmine)-[:DEPENDS { alias: "mysql" }]->(MySQL),
(OwnCloud)-[:DEPENDS { alias: "mysql" }]->(MySQL),

(OpenGTS)-[:DEPENDS { alias: "mysql" }]->(MySQL),
(TracCar)-[:DEPENDS { alias: "mysql" }]->(MySQL),

(Virtuoso)-[:DEPENDS { alias: "mysql" }]->(MySQL),

(iServe)-[:DEPENDS { alias: "rdf" }]->(JenaFuseki),
(iServe)-[:DEPENDS { alias: "rdf" }]->(Virtuoso),
(iServe)-[:DEPENDS { alias: "rdf" }]->(ApacheMarmotta),

(StriderCD)-[:DEPENDS { alias: "mongo" }]->(MongoDB),

(SyslogNG)-[:DEPENDS { alias: "db" }]->(MySQL),
(Sentry)-[:DEPENDS { alias: "db" }]->(MySQL),
(Munin)-[:DEPENDS { alias: "db" }]->(MySQL),

(Logstash)-[:DEPENDS { alias: "es" }]->(ElasticSearch);


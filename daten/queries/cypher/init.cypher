Société	Activité	Contact	Fonction	Email	Site web	logo

CREATE (Morocco:Country { title: "Maroc" }),
       (Nigeria:Country { title: "Nigeria" }),

       (IndustrialExploit:Activity { title: "Exploitation industrielle" }),
       (Telecom:Activity { title: "Télécommunication" }),
       (Banking:Activity { title: "Banques & Finances" }),
       (Ciment:Activity { title: "Fabrication de ciments" }),
       (Raffinerie:Activity { title: "Raffinerie & Transformation" }),

       (MANAGEM:Corporation { title: "MANAGEM Group" }),
       (LAFARGE_HOLCIM:Corporation { title: "LAFARGE-HOLCIM" }),
       (COSUMAR:Corporation { title: "COSUMAR Group" }),

       (MarocTelecom:Company { title: "Maroc Telecom" }),
       (MANAGEM_SA:Company { title: "MANAGEM S.A" }),
       (SM_IMITER:Company { title: "Société Métallurgique d'Imiter" }),
       (BMCE:Company { title: "Banque Marocaine de Commerce Extérieur", website: "www.bmci.ma" }),
       (LAFARGE_CIMENT:Company { title: "LAFARGE Ciments" }),
       (COSUMAR_SA:Company { title: "COSUMAR S.A" }),

(MANAGEM_SA)-[:OWNED_BY]->(MANAGEM),
(LAFARGE_CIMENT)-[:OWNED_BY]->(LAFARGE_HOLCIM),
(COSUMAR_SA)-[:OWNED_BY]->(COSUMAR),

(MarocTelecom)-[:OPERATE_IN]->(Morocco),
(MarocTelecom)-[:ACTIVE_IN]->(Telecom),
(MANAGEM_SA)-[:OPERATE_IN]->(Morocco),
(MANAGEM_SA)-[:ACTIVE_IN { fields: ["mineral"] } ]->(IndustrialExploit),
(SM_IMITER)-[:OPERATE_IN]->(Morocco),
(SM_IMITER)-[:ACTIVE_IN { fields: ["mineral"] } ]->(IndustrialExploit),
(BMCE)-[:OPERATE_IN]->(Morocco),
(BMCE)-[:ACTIVE_IN]->(Banking),
(LAFARGE_CIMENT)-[:OPERATE_IN]->(Morocco),
(LAFARGE_CIMENT)-[:ACTIVE_IN]->(Ciment),
(COSUMAR_SA)-[:OPERATE_IN]->(Morocco),
(COSUMAR_SA)-[:ACTIVE_IN { fields: ["sugar"] } ]->(IndustrialExploit),
(COSUMAR_SA)-[:ACTIVE_IN { plants: ["bettrave"] } ]->(Raffinerie);

#!/bin/bash

export HOSTNAME=`hostname`
export PERSONNA=$USER

export APP_POOL=native
export APP_ROOT=$PWD

########################################################################################

if [[ "x"$PORT == "x" ]] ; then
    export PORT=7000
fi

export REDIS_URL="redis://localhost:6397/0"
export MONGODB_URL="mongodb://localhost/prism"
export NEO4J_URL="http://localhost:7474"

export APP_LIST="common connector console explor linked hobbit linguist devops infosec"

########################################################################################

export APP_AXIS=$APP_ROOT/realms

export APP_CODE=$APP_ROOT/codes
export APP_TEMP=$APP_ROOT/cache
export APP_PROG=$APP_ROOT/progs
export APP_SERV=$APP_ROOT/serve

export APP_CORP=$APP_TEMP/corpora
export APP_VENV=$APP_TEMP/pyenv
export APP_NODE=$APP_PROG/nodejs

export APP_TEMP_NPM=$APP_TEMP/npm

export PYTHONPATH="$APP_CODE/python"
export NODE_PATH="$APP_CODE/nodejs"
export NODE_VERSION="v7.9.0"

if [[ -d $APP_VENV ]] ; then
    source $APP_VENV/bin/activate
fi

export APP_PATH=$APP_ROOT/script:$APP_ROOT/node_modules/.bin
export APP_PATH=$APP_PATH:$APP_NODE/bin

export PATH=$APP_PATH:$PATH

#export NLTK_CORP=`echo book gutenburg {prop,tree}bank {verb,word}net udhr{,2}`
##export NLTK_CORP=`echo conll200{0,2,7} city_database dependency_treebank dolch framenet_v1{5,7} genesis knbc names omw pil propbank pros_cons ptb reuters semcor senseval sentence_polarity sentiwordnet state_union shakespeare subjectivity switchboard toolbox words webtext`
#export NLTK_CORP=`echo book {prop,{,dependency_}tree}bank {name,verb,word}s {verb,{,senti}word}net udhr{,2} conll200{0,2,7} framenet_v1{5,7} city_database dolch genesis gutenburg knbc omw pil pros_cons ptb reuters semcor senseval sentence_polarity state_union shakespeare subjectivity switchboard toolbox webtext`
export NLTK_DATA=$APP_CORP/nltk

########################################################################################

if [[ ! -d $APP_VENV ]] ; then
    virtualenv --distribute $APP_VENV

    $APP_VENV/bin/pip install -r $APP_ROOT/requirements.txt
fi

if [[ -d $APP_VENV ]] ; then
    source $APP_VENV/bin/activate
fi

########################################################################################

if [[ ! -d $APP_NODE ]] ; then
    cd $APP_PROG

    TARGET_ARCH=x64
    #TARGET_ARCH=x86

    TARGET_LINK=https://nodejs.org/dist/${NODE_VERSION}/node-${NODE_VERSION}-linux-$TARGET_ARCH.tar.gz

    curl -# -o ${APP_TEMP_NPM}/node-${NODE_VERSION}-linux-$TARGET_ARCH.tar.gz $TARGET_LINK
    cd ${APP_TEMP_NPM}
    tar xzf node-${NODE_VERSION}-linux-$TARGET_ARCH.tar.gz
    mv ${APP_TEMP_NPM}/node-${NODE_VERSION}-linux-$TARGET_ARCH ${APP_NODE}
    rm node-${NODE_VERSION}-linux-$TARGET_ARCH.tar.gz
fi

export PATH=$APP_PATH:$PATH

#*************************************************************************

env - node=$APP_NODE/bin/node

npm config set tmp $APP_TEMP_NPM
npm config set python $APP_VENV/bin/python2.7

if [[ ! -d "$APP_ROOT/node_modules" ]] ; then
    npm install
fi

env - coffee=$APP_ROOT/node_modules/.bin/coffee

########################################################################################

crython () {
    if [[ ! -d $APP_CODE/python/$2 ]] ; then
        mkdir -p $APP_CODE/python/$2
    fi

    if [[ ! -f $APP_CODE/python/$2/__init__.py ]] ; then
        touch $APP_CODE/python/$2/__init__.py
    fi

    for key in $APP_LIST ; do
        if [[ -d $APP_AXIS/$key ]] ; then
            if [[ -e $APP_CODE/python/$2/$key ]] ; then
                rm -f $APP_CODE/python/$2/$key
            fi

            if [[ -d $APP_AXIS/$key/$1 ]] ; then
                ln -s $APP_AXIS/$key/$1 $APP_CODE/python/$2/$key
            fi

            if [[ -e $APP_CODE/python/$2/$key/$1 ]] ; then
                rm -f $APP_CODE/python/$2/$key/$1
            fi

            if [[ -d $APP_CODE/python/$2/$key ]] ; then
                if [[ ! -f $APP_CODE/python/$2/$key/__init__.py ]] ; then
                    touch $APP_CODE/python/$2/$key/__init__.py
                fi

                case $2 in
                    nucleon|psyapps)
                        ;;
                    *)
                        echo "from . import "$key >>$APP_CODE/python/$2/$key/__init__.py
                        ;;
                esac

                if [[ -f $APP_CODE/python/$2/$key/.gitkeep ]] ; then
                    rm -f $APP_CODE/python/$2/$key/.gitkeep
                fi
            fi
        fi
    done
}

crython "wrap"      nucleon
crython "apps"      psyapps

crython "func/code" bushido
crython "func/flow" enlight
crython "func/bulk" unverse
crython "func/task" dynasty
crython "func/walk" octopus

unset crython

for key in $APP_LIST ; do
    for aspect in dataset ontology process viewer ; do
        if [[ ! -d $APP_SERV/specs/$aspect ]] ; then
            mkdir -p $APP_SERV/specs/$aspect
        fi

        for nrw in `ls $APP_AXIS/$key/spec/$aspect` ; do
            src=$APP_AXIS/$key/spec/$aspect/$nrw
            dest=$APP_SERV/specs/$aspect/$key.$sub

            if [[ -d $src ]] ; then
                if [[ ! -d $dest ]] ; then
                    ln -s $src $dest
                fi
            fi

            case $aspect in
                dataset)
                    ;;
                ontology)
                    ;;
                process)
                    ;;
                viewer)
                    ;;
            esac
        done
    done
done


from reflector.shortcuts import *

################################################################################

VERSEs = {
    'web': dict(
    ),
    'social': dict(
    ),
    'cables': dict(
    ),
    'knowledge': dict(
    ),
    'research': dict(
    ),

    'document': dict(
    ),
    'image': dict(
    ),
    'video': dict(
    ),
}

@Prism.wsgi.route(r'/search/:key/')
def perform_search(key):
    if key in VERSEs:
        pass

    return render_template('special/portals.html', content=[
    ])

################################################################################

@Prism.wsgi.route(r'/search/:key/history')
def history_page():
    return render_template('special/portals.html', content=[
    ])


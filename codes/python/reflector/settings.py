from .routing import *

FUSEKI_SETTINGS = {}

for key in ('connector','console','explor','hobbit','linguist','linked','infosec','devops'):
    FUSEKI_SETTINGS[key] = '%s.kb.uchikoma.loan' % key

MONGODB_SETTINGS = {
    'db': 'neuropsy',
    'host': os.environ['MONGODB_URL']
}

NEO4J_SETTINGS = {
    'db': 'neuropsy',
    'host': os.environ['NEO4J_URL']
}

JOBS = [
    {
        'id': 'job1',
        'func': 'advanced:job1',
        'args': (1, 2),
        'trigger': 'interval',
        'seconds': 10
    }
] ; JOBS = []

SCHEDULER_JOBSTORES = {
    'default': SQLAlchemyJobStore(url='sqlite://')
}

SCHEDULER_EXECUTORS = {
    'default': {'type': 'threadpool', 'max_workers': 20}
}

SCHEDULER_JOB_DEFAULTS = {
    'coalesce': False,
    'max_instances': 3
}

SCHEDULER_API_ENABLED = True

SOCIAL_AUTH_STORAGE = 'social_flask_mongoengine.models.FlaskStorage'


SECRET_KEY = 'random-secret-key'
SESSION_COOKIE_NAME = 'demo_session'
SESSION_PROTECTION = 'strong'

SOCIAL_AUTH_LOGIN_URL = '/'
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/done/'
SOCIAL_AUTH_USER_MODEL = 'example.models.user.User'
SOCIAL_AUTH_AUTHENTICATION_BACKENDS = (
    'social.backends.soundcloud.SoundcloudOAuth2',
    'social.backends.lastfm.LastFmAuth',
)


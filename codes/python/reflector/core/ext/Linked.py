from reflector.core.abstract import *

from surf import *

##########################################################################

@Prism.register('rdf')
class LinkedSurf(Prism.ObjectMapper):
    def prepare(self):
        self._kb = Store(reader='rdflib', writer='rdflib', rdflib_store='IOMemory')

        #self.Document = self._ext.Document
        #self.Embedded = self._ext.EmbeddedDocument

        #self.register_field(str)(self._ext.String)
        #self.register_field(int)(neo4field.Integer)
        #self.register_field(datetime)(neo4field.DateTime)

    store   = property(lambda self: self._kb)

    session = property(lambda self: self.cursor)

    #*********************************************************************

    def connect(self):
        return Session(self._kb)

    ######################################################################

    def load(self, *args, **kwargs): return self.store.load_triples(*args, **kwargs)

    def test_case(self):
        print 'Load RDF data'
        Prism.surf.load(source='http://www.w3.org/People/Berners-Lee/card.rdf')

        Person = Prism.rdf.foaf('Person')

        all_persons = Person.all()

        print 'Found %d persons that Tim Berners-Lee knows'%(len(all_persons))
        for person in all_persons:
            print person.foaf_name.first

        #create a person object
        somebody = Person()
        somebody_else = Person()

        somebody.foaf_knows = somebody_else

##########################################################################

@Prism.register('owl')
class SurfOnOWL(Prism.ObjectMapper):
    def initialize(self):
        pass

    session = property(lambda self: self.manger.surf.session)

    def namespace(self, *args, **kwargs): self.manger.rdf.namespace(*args, **kwargs)
    def prefix(self, *args, **kwargs):    self.manger.rdf.namespace(*args, **kwargs)

    ######################################################################

    def classe(self, narrow):
        return self.manager.surf.session.get_classe(narrow)

    ######################################################################

    def __getitem__(self, narrow):
        return self.session.get_classe(narrow)

    ######################################################################

    Ontology = None

    #*********************************************************************

    def register_ontology(self, *args, **kwargs):
        def do_apply(handler, alias):
            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

##########################################################################

@Prism.register('rdf')
class SurfOnRDF(Prism.ObjectMapper):
    def initialize(self):
        pass

    session = property(lambda self: self.manger.surf.session)

    ######################################################################
    
    def namespace(self, narrow):
        target = getattr(ns, narrow.upper(), None)

        return target
    
    #*********************************************************************

    def prefix(self, narrow, *paths):
        target = self.namespace(narrow)

        if target is None:
            pass

        if target is not None:
            for part in paths:
                target = target[part]

        return target
    
    #*********************************************************************

    def resource(self, uri):
        pass

    ######################################################################
    
    def __getitem__(self, narrow):
        return self.namespace(narrow)

    #*********************************************************************

    def __call__(self, narrow):
        return self.resource(narrow)

    #*********************************************************************

    def __getrange__(self, offset, limit):
        return self.session.get_class(narrow)

    #*********************************************************************

    def __invoke__(self, narrow, *paths): return self.prefix(narrow, *paths)

    ######################################################################

    Predicate = None

    #*********************************************************************

    def register_predicate(self, *args, **kwargs):
        def do_apply(handler, alias):
            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)


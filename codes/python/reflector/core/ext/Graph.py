from reflector.core.abstract import *

from bulbs.model import Node as BulbsNode, Relationship as BulbsEdge
from bulbs.utils import current_datetime

##########################################################################

@Prism.register('neo')
class Neo4j(Prism.ObjectMapper):
    def prepare(self):
        self.register_handler(self.Node, self.register_node)
        self.register_handler(self.Edge, self.register_edge)

        from bulbs import property as neo4field

        self.register_type(str)(neo4field.String)
        self.register_type(int)(neo4field.Integer)
        self.register_type(date)(neo4field.Date)
        self.register_type(datetime)(neo4field.DateTime)

    #*********************************************************************

    def connect(self):
        from bulbs.neo4jserver import Graph

        g = Graph()

        return g

    #*********************************************************************

    graph = property(lambda self: self.cursor)

    ######################################################################

    class Node(BulbsNode):
        pass

    #*********************************************************************

    def register_node(self, *args, **kwargs):
        def do_apply(handler, alias):
            if not hasattr(handler, 'element_type'):
                setattr(handler, 'element_type', alias)

            self._neo.add_proxy("people", Person)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ######################################################################

    class Edge(BulbsEdge):
        pass

    #*********************************************************************

    def register_edge(self, *args, **kwargs):
        def do_apply(handler, alias):
            if not hasattr(handler, 'label'):
                setattr(handler, 'label', alias)

            self._neo.add_proxy("knows", Knows)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)


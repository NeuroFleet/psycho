from reflector.core.abstract import *

##########################################################################

@Prism.register('rrd')
class RoundRobinData(Prism.Extension):
    def initialize(self):
        pass

    #*********************************************************************

    def parse(self, raw, lemmata=True):
        from pattern.en import parsetree

        return parsetree(raw, lemmata=lemmata)

    # [Sentence('big/JJ/B-NP/O white/JJ/I-NP/O rabbit/NN/I-NP/O')]

##########################################################################

@Prism.register('dat')
class DatProtocol(Prism.Extension):
    def initialize(self):
        pass

    #*********************************************************************

    def parse(self, raw, lemmata=True):
        from pattern.en import parsetree

        return parsetree(raw, lemmata=lemmata)

    # [Sentence('big/JJ/B-NP/O white/JJ/I-NP/O rabbit/NN/I-NP/O')]


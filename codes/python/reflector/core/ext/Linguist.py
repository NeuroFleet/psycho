from reflector.core.abstract import *

##########################################################################

@Prism.register('nlp')
class NLP(Prism.Extension):
    def initialize(self):
        from pattern.search import taxonomy

        for f in ('rose', 'lily', 'daisy', 'daffodil', 'begonia'):
            taxonomy.append(f, type='flower')

    #*********************************************************************

    def parse(self, raw, lemmata=True):
        from pattern.en import parsetree

        return parsetree(raw, lemmata=lemmata)

    # [Sentence('big/JJ/B-NP/O white/JJ/I-NP/O rabbit/NN/I-NP/O')]

    #*********************************************************************

    def search(self, raw, qry='NP'):
        from pattern.search import search

        return search(qry, self.nlp_parse(raw))

    # [Match(words=[Word(u'big/JJ')]), Match(words=[Word(u'white/JJ')])]
    # [Match(words=[Word(u'rabbit/NN')])]
    # [Match(words=[Word(u'big/JJ'), Word(u'white/JJ'), Word(u'rabbit/NN')])]

    #*********************************************************************

    def match(self, raw, exp='NP be ADJP|ADVP than NP'):
        from pattern.search import match

        m = match(exp, self.nlp_parse(raw))

        return m

        for w in m.words:
           print w, '\t =>', m.constraint(w)
 
    # Word(u'The/DT')     => Constraint(chunks=['NP'])
    # Word(u'turtle/NN')  => Constraint(chunks=['NP'])
    # Word(u'was/VBD')    => Constraint(words=['be'])
    # Word(u'faster/RBR') => Constraint(chunks=['ADJP', 'ADVP'])
    # Word(u'than/IN')    => Constraint(words=['than'])
    # Word(u'the/DT')     => Constraint(chunks=['NP'])
    # Word(u'hare/NN')    => Constraint(chunks=['NP'])

    #*********************************************************************

    def pattern(self, raw, ptn='{NP} be * than {NP}'):
        t = self.nlp_parse(raw, lemmata=True)

        from pattern.search import Pattern

        p = Pattern.fromstring(ptn)

        m = p.match(t)

        return m

        print m.group(1)
        print m.group(2)
         
        # [Word(u'Chuck/NNP'), Word(u'Norris/NNP')]
        # [Word(u'Dolph/NNP'), Word(u'Lundgren/NNP')]


from reflector.core.abstract import *

##########################################################################

@Prism.register('mongo')
class MongoDB(Prism.ObjectMapper):
    def prepare(self):
        from flask_mongoengine import MongoEngine

        self._ext = MongoEngine(Prism.wsgi)

        self.Document = self._ext.Document
        self.Embedded = self._ext.EmbeddedDocument

        self.register_handler(self.Document, self.register_document)
        self.register_handler(self.Embedded, self.register_embedded)

        self.register_type(str)(self._ext.StringField)
        self.register_type(int)(self._ext.IntField)

        self.register_type(date)(self._ext.DateTimeField)
        self.register_type(datetime)(self._ext.DateTimeField)

    #*********************************************************************

    def connect(self):
        return self._ext

    ######################################################################

    def listing(self, nested, *args, **kwargs):
        return self._ext.ListField(nested, *args, **kwargs)

    #*********************************************************************

    def embed(self, *args, **kwargs):
        return self._ext.EmbeddedDocumentField(*args, **kwargs)

    def foreign(self, *args, **kwargs):
        return self._ext.ReferenceField(*args, **kwargs)

    ######################################################################

    Embedded = None
    Document = None

    #*********************************************************************

    def register_embedded(self, *args, **kwargs):
        def do_apply(handler, alias):
            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #*********************************************************************

    def register_document(self, *args, **kwargs):
        def do_apply(handler, alias):
            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ######################################################################

    def document_form(self, target):
        PostForm = model_form(Post)

        def add_post(request):
            form = PostForm(request.POST)
            if request.method == 'POST' and form.validate():
                # do something
                redirect('done')
            return render_template('add_post.html', form=form)


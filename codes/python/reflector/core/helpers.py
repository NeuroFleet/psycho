from reflector.libs import *

from os.path import abspath,dirname,join as pathjoin

#*******************************************************************************

bpath = dirname(dirname(dirname(dirname(dirname(__file__)))))
rpath = lambda *x: pathjoin(bpath, *x)

#*******************************************************************************

def singleton(handler):
    return handler()

##########################################################################

class Component(object):
    def __init__(self, *args, **kwargs):
        self.trigger('initialize', *args, **kwargs)

    def trigger(self, method_name, *args, **kwargs):
        hnd = getattr(self, method_name, None)

        if callable(hnd):
            return hnd(*args, **kwargs)
        else:
            return None

#*************************************************************************

class Manageable(Component):
    def __init__(self, mgr, *args, **kwargs):
        self._mgr = mgr

        super(Manageable, self).__init__(*args, **kwargs)

    manager = property(lambda self: self._mgr)

##########################################################################



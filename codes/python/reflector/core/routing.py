from .helpers import *

###########################################################################

class DynamicPath(object):
    FIELDs = ('scheme','user','host','path','query')

    def __init__(self, route, handler, pattern):
        for key in self.FIELDs:
            pattern[key] = pattern.get(key, None)

        self._drt = route
        self._hnd = handler
        self._ptn = pattern

    route   = property(lambda self: self._drt)
    handler = property(lambda self: self._hnd)
    pattern = property(lambda self: self._ptn)

    #*********************************************************************

    def clean(self, field):
        flag = self.pattern.get(field, None)

        if flag is None:
            flag = []

        if hasattr(flag, 'keys'):
            flag = getattr(flag, 'keys')

            if callable(flag):
                flag = flag()

        if type(flag) in [tuple,set,frozenset,list]:
            return [x for x in flag]
        else:
            return [x]

    #*********************************************************************

    def match(self, link):
        return reduce(operator.and_, [
            self.check(link, field) for field in self.FIELDs
        ])

    def check(self, link, field):
        valu = getattr(link, field, None)

        if flag is None:
            return True
        elif type(self.pattern[field]) in (tuple,set,frozenset,list):
            if valu in self.pattern[field]:
                return True

        return False

class DynamicRoute(object):
    def __init__(self, router):
        self._prn = rtr
        self._reg = []

        self.wiring()

    router   = property(lambda self: self._prn)
    patterns = property(lambda self: self._reg)

    def route(self, handler, **pattern):
        obj = DynamicPath(self, handler, pattern)

        self._reg.append(obj)

        return obj

    all_handlers = property(lambda self: [
        pattern.handler
        for pattern in self.patterns
    ])

    all_schemes = property(lambda self: [
        ns
        for pattern in self.patterns
        for ns in pattern.clean('scheme')
    ])
    all_hosts = property(lambda self: [
        ns
        for pattern in self.patterns
        for ns in pattern.clean('host')
    ])
    all_users = property(lambda self: [
        ns
        for pattern in self.patterns
        for ns in pattern.clean('user')
    ])

    def resolve(self, uri):
        orig = urlparse(uri)

        link = self.routing(link) or link

        path = []

        for route in self.patterns:
            if route.match(link):
                path.append(route)

        for route in path:
            try:
                target = route.handler(uri)
            except:
                target = None

            if target is not None:
                response = self.serving(orig, target, link)

                if response is None:
                    response = target

                yield route, response

#**************************************************************************

class DynamicRouter(object):
    def __init__(self, mgr):
        self._mgr = mgr
        self._reg = {}

    routes = property(lambda self: self._reg)

###########################################################################

class GenericPath(DynamicRoute):
    def wiring(self):
        self.route(None, scheme=['http','https'])

    def routing(self, link):
        return link

    def serving(self, link, handler, target):
        return handler

#**************************************************************************

class FilesystemPath(DynamicRoutable):
    def wiring(self):
        self.route(None, scheme=['file'])
        self.route(None, scheme=['ftp','sftp','ssh'])

        #self.route(None, scheme=['s3'])
        #self.route(None, scheme=['dav','webdav'])
        #self.route(None, scheme=['ldp'])

        self.route(None, scheme=['ipfs'])

    def routing(self, link):
        return link

    def serving(self, link, handler, target):
        return handler

#**************************************************************************

class FeedPath(DynamicRoutable):
    def wiring(self):
        self.route(None, scheme=['amqp'])
        self.route(None, scheme=['mqtt'])
        self.route(None, scheme=['wamp'])

        self.route(None, scheme=['nntp'])
        self.route(None, scheme=['rss','atom','news'])

    def routing(self, link):
        return link

    def serving(self, link, handler, target):
        return handler

#**************************************************************************

class DataPath(DynamicRoutable):
    def wiring(self):
        self.route(None, scheme=['memcache','redis'])
        self.route(None, scheme=['mysql','postgres'])
        self.route(None, scheme=['mongodb'])
        self.route(None, scheme=['allegro','neo4j'])
        self.route(None, scheme=['xsd','owl','rdf'])
        self.route(None, scheme=['rrd'])
        self.route(None, scheme=['csv','json'])
        self.route(None, scheme=['hdf','hdf5'])

    def routing(self, link):
        return link

    def serving(self, link, handler, target):
        return handler

#**************************************************************************

class ApiPath(DynamicRoutable):
    def wiring(self):
        for key in []:
            ns = ['api']

            if False : # Social
                ns += ['contacts','inbox','timeline']

            if False : # Media
                ns += ['photos','videos']

            self.route(None, scheme=ns, host=[key])

    def routing(self, link):
        return link

    def serving(self, link, handler, target):
        return handler


from .helpers import *

##########################################################################

class Renderable(Manageable):
    def __init__(self, mgr, narrow, title, *params, **attrs):
        self._nrw = narrow
        self._lbl = title

        self._hdn = None
        self._prm = params
        self._kws = attrs

        super(Renderable, self).__init__(mgr)

    narrow   = property(lambda self: self._nrw)
    title    = property(lambda self: self._lbl)
    heading  = property(lambda self: self._hdn or '')

    params   = property(lambda self: self._prm)
    attrs    = property(lambda self: self._kws)

    def __getitem__(self, key, default=None): return self._kws.get(key, default)

    def render(self):
        tpl,cnt = None, self.context

        try:
            tpl = Prism.wsgi.jinja_env.get_template(self.template)
        except TemplateNotFound,ex:
            return ""
        else:
            cnt['wdg']    = self
            cnt['widget'] = self

            cnt['data']   = self

            return tpl.render(cnt)

#*************************************************************************



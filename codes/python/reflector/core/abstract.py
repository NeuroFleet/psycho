from .primitives import *

##########################################################################

class BaseManager(Manageable):
    def __init__(self, *args, **kwargs):
        super(BaseManager, self).__init__(*args, **kwargs)



    def register_handler(self, target, callback):
        if target not in self.manager._reg:
            self.manager._reg[target] = callback

        return self

#*************************************************************************

@singleton
class Prism(object):
    def __init__(self):
        self._ext = {}
        self._reg = {}

        self._mem = redis.StrictRedis(host='localhost', port=6379, db=0)

        self._app = None

    cache = property(lambda self: self._mem)

    @property
    def wsgi(self):
        if self._app is None:
            self._app = Flask(__name__)
            self._app.debug = True

            self._app.root_path     = bpath
            self._app.instance_path = bpath

            self._app.static_url_path = '/static'

            self._app.template_folder = rpath('serve', 'views')
            self._app.static_folder   = rpath('serve', 'assets')

        return self._app

    #*********************************************************************

    bpath = property(lambda self: bpath)

    def rpath(self, *x): return self.rpath(*x)

    #*********************************************************************

    def register(self, *args, **kwargs):
        def do_apply(handler, alias, *args, **kwargs):
            if issubclass(handler, BaseManager):
                obj = getattr(self, alias, None)

                if obj is None:
                    obj = handler(self, *args, **kwargs)

                if alias not in self._ext:
                    self._ext[alias] = obj

                if kwargs.get('state', True)==True:
                    if not hasattr(self, alias):
                        setattr(self, alias, obj)
            else:
                for target in self._reg:
                    if issubclass(handler, target):
                        callback = self._reg[target]

                        return callback(alias, *args, **kwargs)(handler)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ######################################################################

    class Aspect(BaseManager):
        pass

    ######################################################################

    class Extension(BaseManager):
        pass

    #*********************************************************************

    class ObjectMapper(BaseManager):
        def initialize(self):
            self._cnx = None

            self._reg = {
                'field': {},
            }

            self.trigger('prepare')

        @property
        def cursor(self):
            if self._cnx:
                self._cnx = self.connect()

            return self._cnx

        ######################################################################

        def register_type(self, *args, **kwargs):
            def do_apply(handler, alias):
                if alias not in self._reg['field']:
                    self._reg['field'][alias] = handler

                return handler

            return lambda hnd: do_apply(hnd, *args, **kwargs)

        #*********************************************************************

        def field(self):
            pass


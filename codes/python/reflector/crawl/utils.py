from __future__ import with_statement

from .abstract import *

################################################################################

class CrawlEngine(object):
    def __init__(self, namespace, domains=[], targets=[], config={}, rules=[]):
        self._ns  = namespace
        self._dns = domains
        self._trg = targets
        self._cfg = config
        self._rul = []

        for item in rules:
            self._rul.append(WebRule(self, **item))

        self.reset()

    namespace = property(lambda self: self._ns)
    domains   = property(lambda self: self._dns)
    targets   = property(lambda self: self._trg)
    config    = property(lambda self: self._cfg)
    rules     = property(lambda self: self._rul)

    #***************************************************************************

    def trigger(self, method, *args, **kwargs):
        handler = getattr(self, method, None)

        if callable(handler):
            return handler(*args, **kwargs)
        else:
            return None

    #***************************************************************************

    def reset(self):
        self._see = set()

        self._pol = eventlet.GreenPool()

    def start(self):
        self.reset()

        for target in self.targets:
            self.queue(target)

        self._pol.waitall()

        return self._see

    #***************************************************************************

    def queue(self, link):
        if type(link) in (str, unicode):
            link = WebLink(link)

        if link.url not in self._see:
            self._see.add(link.url)

            self._pol.spawn_n(self.fetch, link)

    def fetch(self, link):
        doc = WebFile.retrieve(self, link)

        if doc is not None:
            for r in self.rules:
                r.match(doc)

    #***************************************************************************

    def get_links(self, doc):
        for url_match in url_regex.finditer(doc.source):
            new_url = url_match.group(0)

            if new_url.startswith('javascript'):
                new_url = ''

            if len(new_url):
                yield new_url

        for anchor in doc.css('a'):
            link = anchor.attributes.get('href','')

            if link.startswith('javascript'):
                link = ''

            if '://' not in link:
                link = urljoin(link, doc.link.url)

            if len(link):
                yield link

    #***************************************************************************

    def __call__(self):
        self.run()


from .libs import *

#*******************************************************************************

# http://daringfireball.net/2009/11/liberal_regex_for_matching_urls
url_regex = re.compile(r'\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))')

################################################################################

class WebLink(object):
    def __init__(self, target, refer=None):
        self._trg = target
        self._ref = refer
        self._lnk = urlparse(self.url)

    url      = property(lambda self: self._trg)
    referrer = property(lambda self: self._ref)
    link     = property(lambda self: self._lnk)

    scheme   = property(lambda self: self.link.scheme)
    username = property(lambda self: self.link.username)
    password = property(lambda self: self.link.password)
    hostname = property(lambda self: self.link.hostname)
    port     = property(lambda self: self.link.port)
    path     = property(lambda self: self.link.path)
    query    = property(lambda self: self.link.query)

    def match(self, pattern):
        index,args,kwargs = 0,[],{}

        for field,value in [
            ('host', self.hostname),
            ('path', self.path[1:]),
        ]:
            if field in pattern:
                m = re.match(pattern[field], value)

                if m:
                    for item in m.groups():
                        args.append(item)

                    for k,v in m.groupdict().iteritems():
                        kwargs[k] = v

                    index += 1

        if index==len(pattern.keys()):
            return args,kwargs
        else:
            return None

    def __str__(self):     return str(self.url)
    def __unicode__(self): return unicode(self.url)

#*******************************************************************************

class WebRule(object):
    def __init__(self, engine, narrow=None, follow=False, allows=[], forbid=[], models=[]):
        self._eng = engine
        self._nrw = narrow
        self._flw = follow
        self._yes = allows
        self._den = forbid
        self._mdl = models

    engine = property(lambda self: self._eng)

    narrow = property(lambda self: self._nrw)
    follow = property(lambda self: self._flw)

    allows = property(lambda self: self._yes)
    forbid = property(lambda self: self._den)

    models = property(lambda self: self._mdl)

    def banned(self, doc):
        flag = False

        for pattern in self.forbid:
            resp = doc.match(pattern)

            if resp is not None:
                flag = True

        return flag

    def match(self, doc):
        if doc.link.hostname in self.engine.domains:
            valid = []

            for pattern in self.allows:
                resp = doc.match(pattern)

                if resp is not None:
                    if self.follow:
                        listing = [x for x in self.engine.get_links(doc)]

                        # print "Checking '%d' follow links from : %s" % (len(listing), doc.link.url)

                        for url in listing:
                            if url not in self.engine._see:
                                target = WebLink(url, refer=doc.link)

                                if target.hostname in self.engine.domains:
                                    self.engine.queue(target)

                    resp = dict(
                        incr=resp[0],
                        name=resp[1],
                    )

                    for schema in self.models:
                        self.engine.trigger('extract', doc, schema, resp)

                    valid.append(pattern)

            if self.narrow is not None:
                if len(valid)==0:
                    pass # print "\t#) No match for : %s" % doc.link.url
                else:
                    pass # print "\t=> Found match for '%s' with patterns :" % doc.link.url

                    #for ptn in valid:
                    #    print "\t\t*) %s" % ptn


from .helpers import *

FA_MAPPING = {
    'connector': 'plug',
    'console':   'building',
    'devops':    'code',
    'linguist':  'code',
    'linked':    'connectdevelop',
}

################################################################################

@Prism.wsgi.context_processor
def inject_user():
    return dict(hello='world')

#*******************************************************************************

@Prism.wsgi.context_processor
def utility_processor():
    def static_assets(*path):
        return '/'.join(['/static']+[x for x in path])

    def format_price(amount, currency=u'$'):
        return u'{0:.2f}{1}'.format(amount, currency)

    def font_awesome(src):
        dest = src

        while dest in FA_MAPPING:
            dest = FA_MAPPING[dest]

        return dest

    return dict(
        currency = format_price,
        static   = static_assets,
        fa_icon  = font_awesome,
    )

################################################################################

@Prism.wsgi.template_filter('static')
def static_assets(*path):
    return '/'.join(['/static']+[x for x in path])

#*******************************************************************************

@Prism.wsgi.template_filter('currency')
def format_price(amount, currency=u'$'):
    return u'{0:.2f}{1}'.format(amount, currency)

#*******************************************************************************

@Prism.wsgi.template_filter('fa_icon')
def font_awesome(src):
    dest = src

    while dest in FA_MAPPING:
        dest = FA_MAPPING[dest]

    return dest

################################################################################

from social_flask.routes import social_auth

Prism.wsgi.register_blueprint(social_auth, url_prefix="/admin/people/")

#*******************************************************************************

import rq_dashboard

Prism.wsgi.register_blueprint(rq_dashboard.blueprint, url_prefix="/admin/queues/")

#*******************************************************************************

from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from flask_apscheduler import APScheduler

scheduler = APScheduler()
scheduler.init_app(Prism.wsgi)
scheduler.start()


from .libs import *

from .shortcuts import Prism

from . import views
from . import apis
from . import dashs

app = Prism.wsgi

if __name__ == "__main__":
    app.run()


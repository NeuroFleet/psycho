from .libs import *
from .core.helpers import *

#*******************************************************************************

from .core.abstract import *

################################################################################

Prism.wsgi.jinja_options = {
    'extensions': [
        'jinja2.ext.autoescape',
        'jinja2.ext.do',
        'jinja2.ext.loopcontrols',
        'jinja2.ext.with_',
    ],
}


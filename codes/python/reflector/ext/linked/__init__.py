from reflector.shortcuts import *


from flask_restful import Resource

class Foo(Resource):
    def get(self):
        pass
    def post(self):
        pass

def fuseki_sparql(host, query, dataset='ds'):
    try:
        req = requests.post('http://%s/%s/query' % (host,dataset), data={
            'query': query,
        })

        resp = req.json()
    except Exception,ex:
        print ex

        return None
    else:
        return resp['results']['bindings']

def fuseki_status(host, dataset='ds', details=False):
    verse,sparql,lookup = 'simple',{
        'triples':    dict(select='*', where='?s ?p ?o .'),
        'ontologies': dict(select='distinct ?ontology', where='?subject rdf:type ?ontology . }'),
        'predicates': dict(select='distinct ?predicate', where='?subject ?predicate ?object . }'),
    },'select (count(%(select)s) as ?count) where { %(where)s }'

    if details:
        lookup = 'select (%(select)s) where { %(where)s }'
        verse  = 'details'

    resp = {}

    for field in sparql:
        nrw = 'rdf://status@%s/%s/%s' % (host,verse,field)

        if cache.get(nrw) is None:
            query = lookup % sparql['field']

            resp[field] = fuseki_sparql(host, sparql[field], dataset=dataset)

            if resp[field] is None:
                resp[field] = 0
            else:
                resp[field] = int(resp[field][0]['count']['value'])

            cache.set(nrw, resp[field], ex=30)
        else:
            resp[field] = cache.get(nrw)

    return resp

@Prism.wsgi.route("/api/rdf/stores/status.json")
def api_rdf_store_global():
    resp = {}

    for key in (
        #'connector','console',
        'devops','infosec',
        'linked','linguist',
        'explor','hobbit',
    ):
        resp[key] = fuseki_status('%s.kb.uchikoma.loan' % key)

    return render_json(resp)

@Prism.wsgi.route("/api/rdf/stores/:narrow/")
def api_rdf_store_local(narrow):
    return render_json(fuseki_status('%s.kb.uchikoma.loan' % narrow))


#!/usr/bin/env python

import click, os, sys, re, json

rpath = lambda *x: os.path.join(os.path.dirname(__file__), *x)

#***************************************************************************************

default_port = 7000

#***************************************************************************************

from reflector.crawl.shortcuts import *

########################################################################################

def cross_worker(**cfg):
    return [{
        "type": "guest",
        "executable": cfg['prog'],
        "arguments": [str(x) for x in cfg.get('args', [])],
        "options": {
            "workdir": cfg.get('wdir', '../'),
            "env": {
                "inherit": True,
                "vars": dict([(str(k),str(v)) for k,v in cfg.get('vars', {}).iteritems()]),
            },
            "watch": {
                "directories": cfg.get('spwn', []),
                "action": "restart"
            }
        }
    }]

#***************************************************************************************

@click.group()
@click.option('--port', default=None)
@click.pass_context
def cli(ctx, port, *args, **kwargs):
    try:
        port = int(port or os.environ.get('PORT', default_port))
    except:
        print "Supplied port '%s' not conform. Using '%s' instead."

        port = default_port

    reg = {}

    for key in ('realm','comps','paths','hooks','socks','progs'):
        reg[key] = json.loads(open(rpath('amber','cross','%s.json' % key)).read())

    from multiprocessing import Pool

    ctx.obj.update({
        'cross': {
            'parts':  reg,
            'worker': cross_worker,
        },
        'pool': Pool(),
        'port':   dict(
            http = port,
            sock = port+1,
            flsh = port+2,
            wsgi = port+3,

            job  = port+5,

            tpf  = port+10,
            ldp  = port+11,
        ),
    })

########################################################################################

def shell_dir_exts(*args, **kwargs):
    def get_paths(filename):
        for aspect in ('amber','daten','ghost'):
            yield rpath(aspect, filename)

        for key in ('m','l','n','s'):
            yield rpath('amber', '%sight' % key, filename)

        for app in os.listdir(rpath('realms')):
            yield rpath('realms', app, 'func', filename)

    def do_apply(handler, filename, glob, locl):
        for path in get_paths('shell.py'):
            if os.path.exists(path):
                print "\t-> %s : %s" % (filename,path)

                handler(path, filename, glob, locl)

    return lambda hnd: do_apply(hnd, *args, **kwargs)

#***************************************************************************************

@shell_dir_exts('shell.py', globals(), locals())
def load_shell_exts(path, spec, glob, locl):
    source = open(path).read()

    try:
        exec source in glob, locl
    except Exception,ex:
        print "Error raised while parsing : %s" % path

        raise ex

########################################################################################

if __name__ == '__main__':
    cli(
        obj={
            'debug': True,
            #'key': 'value',
            'python': '/usr/bin/python',
            'pip':    '/usr/bin/pip',
        }
    )


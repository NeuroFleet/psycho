#!/usr/bin/env python

import click, os, sys, re, json, yaml, operator

rpath = lambda *x: os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)

#***************************************************************************************

default_port = 7000

#***************************************************************************************

from reflector.crawl.shortcuts import *

########################################################################################

@click.group()
@click.pass_context
def cli(ctx, *args, **kwargs):
    ctx.obj['realms'] = os.environ.get('APP_LIST','').split(' ')

    if len(ctx.obj['realms'])==0:
        ctx.obj['realms'] = [
            key
            for key in os.listdir(rpath('realms'))
            if os.path.isdir(rpath('realms',key))
        ]

    art,dok = {},{}

    for key in ctx.obj['realms']:
        art[key] = yaml.load(open(rpath('realms',key,'artefact.yaml')))
        dok[key] = yaml.load(open(rpath('realms',key,'contains.yaml')))

    def enrich_hdt(meta):
        if ('link' not in meta) and ('base' in meta) and ('file' in meta):
            meta['link'] = meta['base'] + meta['file']

            if 'arch' in meta:
                meta['link'] += '.' + meta['arch']

        meta['path'] = rpath('daten','linked','hdt',meta['file'])

        return meta

    reg = {
        'records': {
            'hdt': [
                enrich_hdt(meta)
                for key,cfg in art.iteritems()
                for meta in cfg.get('corporas',{}).get('tpf',{})
                if meta.get('type', 'unknown') in ['hdt']
            ],
        },
        'contain': {
            'docker': [
                obj
                for spec in dok.values()
                for lst in spec.values()
                for obj in lst
                if ('image' in obj)
            ],
        },
        'corpora': {
            'nltk': [
                corp
                for key,obj in art.iteritems()
                for corp in cfg.get('corporas',{}).get('nltk',{})
            ],
        },
        'package': {
            'python': [
                pkg
                for spec in art.values()
                for lst in [
                    spec.get('dependencies',{}).get('python',[]),
                    spec.get('requirements',{}).get('python',[]),
                ]
                for pkg in lst
            ],
            'nodejs': [
                pkg
                for spec in art.values()
                for lst in [
                    spec.get('dependencies',{}).get('nodejs',[]),
                    spec.get('requirements',{}).get('nodejs',[]),
                ]
                for pkg in lst
            ],
        },
    }

    for key in os.environ.get('NLTK_CORP','book verbnet wordnet').split(' '):
        if key not in reg['corpora']['nltk']:
            reg['corpora']['nltk'].append(key)

    for key in ('compose','backup'):
        for nrw in art.get(key, {}):
            img = art[key][nrw]['image']

            if img not in reg['contain']['docker']:
                reg['contain']['docker'].append(img)

    ctx.obj.update(reg)

    from multiprocessing import Pool

    ctx.obj.update({
        #'doms': dns,
        'pool': Pool(20),
    })

########################################################################################

@cli.command('cache')
@click.option('--nltk', default=False)
@click.option('--hdt', default=True)
@click.option('--docker', default=False)
@click.pass_context
def download_files(ctx, **opts):
    print "Downloading cache files :"

    if opts.get('hdt', False):
        print "\t*) HDT datasets :"

        target = rpath('daten','linked','hdt')

        if not os.path.exists(target):
            os.system('mkdir -p %s' % target)

        os.chdir(target)

        for meta in ctx.obj['records']['hdt']:
            if not os.path.exists(meta['path']):
                print "\t\t-> %(path)s ..." % meta

                os.system('gnome-terminal -e "wget -c %s"' % meta['link'])

        os.chdir(rpath())

    import nltk

    if opts.get('nltk', False):
        print "\t*) NLTK corporas :"

        for corp in ctx.obj['corpora']['nltk']:
            print "\t\t-> %s ..." % corp

            nltk.download(corp)

    if opts.get('docker', False):
        print "\t*) Docker images :"

        for container in ctx.obj['contain']['docker']:
            print "\t\t-> %(image)s ..." % container

            os.system('docker pull %(image)s' % container)

#***************************************************************************************

@cli.command('install')
#@click.option('--node-js', default=False)
#@click.option('--node-version', default='v7.9.0')
#@click.option('--neo4j', default=False)
#@click.option('--elastic', default=False)
#@click.option('--mqtt', default=False)
@click.pass_context
def install_programs(ctx, *args, **kwargs):
    if kwargs['node-js']:
        if not exists(rpath('progs','nodejs')):
            os.chdir(rpath('progs'))

#***************************************************************************************

@cli.command('build')
@click.option('--docker', default=False)
@click.pass_context
def build_artefacts(ctx, *args, **kwargs):
    if kwargs['docker']:
        print "\t*) Docker images :"

        for container in ctx.obj['contain']['docker']:
            print "\t\t-> %(image)s ..." % container

            # os.system('docker pull %(image)s' % container)

#***************************************************************************************

@cli.command('setup')
@click.pass_context
def setup_commands(ctx, *args, **kwargs):
    pass

#***************************************************************************************

@cli.command('sync')
@click.option('--secret', default='')
@click.pass_context
def sync_ghost(ctx, *args, **kwargs):
    pass

########################################################################################

#@shell_dir_exts('shell.py', globals(), locals())
def load_shell_exts(path, spec, glob, locl):
    source = open(path).read()

    try:
        exec source in glob, locl
    except Exception,ex:
        print "Error raised while parsing : %s" % path

        raise ex

########################################################################################

if __name__ == '__main__':
    cli(
        obj={
            'debug': True,
            #'key': 'value',
            'python': '/usr/bin/python',
            'pip':    '/usr/bin/pip',
        }
    )

